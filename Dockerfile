FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g-dev
RUN wget https://nginx.org/download/nginx-1.20.2.tar.gz && tar xvfz nginx-1.20.2.tar.gz && cd nginx-1.20.2 && ./configure --sbin-path=/usr/local/nginx/sbin/nginx --conf-path=/usr/local/nginx/conf/nginx.conf --pid-path=/usr/local/nginx/nginx.pid && make && make install 

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx

EXPOSE 80
CMD ["./nginx", "-g", "daemon off;"]
